.. chatbot-plzen-doc documentation master file, created by
   sphinx-quickstart on Mon Jul  1 06:33:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to chatbot-plzen-doc's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   content/uvod
   content/architektura
   content/chatbot_diagram
   content/facebook
   content/faq
   content/contact


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
