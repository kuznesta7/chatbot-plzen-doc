###################
Úvod
###################

Vítáme Vás na stránkách dokumentace projektů „Chatbot Města Plzeň“.

Chatbot WDF je software využívající principy umělé inteligence. Je určený k automatizované
komunikace s lidmi prostřednictvím textových zprav. Kumunikovat umí prostřednictvím řady
komunikačních kanálů (Facebook Messenger, whatsapp, slack, skype, vlastní okno chatu).
Největší výhodou je škálovatelnost, kde chatbot dokáže komunikovat s velkým množstvím lidi
najednou bez větších prodlev a se stejnou kvalitou.

---------------------------
Napojené komunikační kanály
---------------------------

V rámci implementace chatbota pro město Plzeň bude pro uživatele veškerá logika a celá
komunikace dostupná ve dvou komunikačních kanálech. Tyto kanály nebudou vzájemně
propojeny a komunikace na nich bude zcela oddělená. Historie komunikace se bude
uchovávat jen pro messenger (jako pro běžnou uživatelskou komunikaci).

---------------------------
Messenger
---------------------------

Uživatele budou mít možnost začít s chatbotem komunikovat přímo přes messenger (chatbot
bude mít své jméno). Tuto komunikaci může uživatel zahájit i přes facebook stránky.
Tímto komunikačním kanálem se také budou rozesílat případné novinky.

-----------------------------------------
Vlastní okno chatu na webových stránkách
-----------------------------------------

Návštěvník stránek bude mít možnost bez jakékoliv autentifikace začít s chatbotem
komunikovat. Chatbot bude disponovat veškerou logikou a inteligencí, kterou bude mít ve
variantě pro messeger.

-----------
Small talk
-----------

Small talk slouží především pro odlehčení komunikace a zlidštění chatbota.

1. Půjde o několik reakcí na případy, kdy chatbot nebude rozumět otázce a nabídne
uživateli nějaké základní možnosti, případně se bude týkat určité oblasti, ve které
uživatel práve v rámci flow bude.

2. Dále několik obecných zajímavostí a s tématem nesouvisejících sdělení pro případy,
kdy uživatel přestane komunikovat (po časové prodlevě).

3. Chatbota naučíme i několik “vtipných” odpovědí. Toto poslouží především pro
pobavení a pro virální šíření mezi lidmi.

--------------------------
Scénáře životních situací
--------------------------

Chatbot bude uživatele navigovat stromem, který se bude skládat ze 4 hlavních agend, dále
rozložených na cca 40 konkrétních situací, každá situace na další části. Jednotlivé větve navíc
můžou mít své odlišnosti a nabízet uživateli jiné možnosti.

------------------------------
Seznam všech životních situací
------------------------------

Občanský průkaz, cestovní pas

* Vydání občanského průkazu
* Vydání cestovních dokladů

Evidence občanů

* Hlášení trvalého pobytu
* Rušení trvalého pobytu
* Hlášení adresy pro doručování
* Výdej údajů z evidence obyvatel a zprostředkování kontaktu

Oblast registru řidičů a vozidel

* Vydání řidičského průkazu
* Vydání mezinárodního řidičského průkazu
* Vydání paměťové karty vozidla
* Vydání paměťové karty řidiče
* Vydání paměťové karty dílny
* Výpis z evidenční karty řidiče
* Žádost o přestavbu silničního vozidla
* Žádost o zápis vozidla do registru silničních vozidel
* Žádost o zápis změny vlastníka nebo provozovatele
* Žádost o vyřazení silničního vozidla z provozu
* Žádost zápisu zániku silničního vozidla
* Žádost o povolení výroby jednotlivého silničního vozidla a rozhodnutí o technické způsobilosti jednotlivě vyrobeného silničního vozidla
* Žádost o schválení technické způsobilosti jednotlivě dovezeného vozidla
* Žádost o zápis historického a sportovního vozidla do registru historických a sportovních vozidel a o uznání testování silničního vozidla
* Žádost o poskytnutí údajů z registru silničních vozidel
* Výpis záznamů bodového hodnocení z registru řidiče
* Oznámení o ztrátě, zničení nebo odcizení tabulky s přidělenou registrační značkou
* Žádost o vydání zvláštní registrační značky pro manipulační provoz
* Žádost o vydání zvláštní registrační značky za účelem jízdy z místa prodeje do místa registrace
* Žádost o zápis změn ostatních údajů v registru silničních vozidel
* Žádost o vydání tabulky s registrační značkou na vývoz
* Žádost o vydání nového dokladu (z důvodu ztráty, odcizení, poškození či zničení)
* Žádost o vydání tabulky s registrační značkou (z důvodu poškození tabulky s přidělenou registrační značkou nebo registrační značkou na přání)
* Žádost o vydání tabulky s registrační značkou (k umístění na nosné zařízení připojitelné k silničnímu vozidlu)
* Žádost o přidělení registrační značky na přání
* Žádost o rezervaci registrační značky na přání

Živnostenský úřad

* Žádost o koncesi pro právnické osoby
* Žádost o koncesi pro fyzické osoby
* Ohlášení živnosti pro fyzické osoby
* Ohlášení živnosti pro právnické osoby
* Zemědělský podnikatel - Žádost při z.č. 252/1997 sb., o zemědělství, v pl. znění, o zápisu do evidence zemědělského podnikatele
* Jednotné kontaktní místo - podnikání v EU, přeshraniční poskytování služeb