#############################
Facebook Testovací prostředí
#############################


---------------------------
Facebook page
---------------------------
.. note:: Facebook page

**Adresa:** https://www.facebook.com/Chatbot-M%C4%9Bsta-Plze%C5%88-634459717025233/

**Popis**

.. _fb_chatbot_funpage_test:
.. figure:: images/fb_chatbot_funpage_test2.jpg
    :align: center

---------------------------
Facebook developer
---------------------------
.. note:: Facebook developer

**Adresa:** https://developers.facebook.com/apps/2497312107157020/dashboard/?business_id=320531875305467

**Popis**

.. _fb_developer_test:
.. figure:: images/fb_developer_chatbot_test2.jpg
    :align: center



---------------------------
Facebook business
---------------------------
.. note:: Facebook business

**Adresa:** https://www.facebook.com/business

**Popis**

.. _fb_business_test:
.. figure:: images/fb_business_test2.jpg
    :align: center