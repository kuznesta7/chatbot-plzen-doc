###################
Deploy
###################

---------------------------
HW Požadavky
---------------------------
* 2 vCPU
* min 16GB ram
* 64GB disk SSD
* Private IP
* Ubuntu 16.04 or more

---------------------------
Postup nasazení
---------------------------
**GIT**
Nasazení aplikace se provádí automaticky, pomocí GitRunner_ a zrcadlení Git repositářů.

Zavádíme celkem 2 přístupy pro deploy tzv. Test production obr a Hot Fix

Postup pro Test production je znázorněn na obrázku test_production_.

* Vývojář pro každé Issue bude zakládat novou branch
* Po otestování na testovacím serveru udělá commit do WDF Git repositáře
* Ve WDF Git repositáři se provede Merge request do test_production branch
* Spustí se Git runner
* Test production se automaticky nahraje do Test production brach Git repositáře města Plzeň
* Odpovědna osoba (admin) provede deploy na Dev Test a provede testovaní/kontrolu
* Pokud test proběhl v pořádku, provede admin merge reauest s master branch
* V poslední fázi provede admin deploy na produkční server
* Pokud test na Dev Test se provedl špatně, admin zahodí poslední změny a pošle report s popisem chyby

Adresa dev serveru:

* dev.chatbot.plzen.eu
* dev.chatbot.plzen.eu/admin


.. _test_production:
.. figure:: images/wdf_deploy_test_production.jpg
    :align: center

    Test production

Postup pro Hot Fix je znázorněn na obrázku hot_fix_.

* Vývojář pro každé Issue bude zakládat novou branch
* Po otestování na testovacím serveru udělá commit do WDF Git repositáře
* Ve WDF Git repositáři se provede Merge request do Hot Fix branch
* Spustí se Git runner
* Hot fix se automaticky nahraje do Hot Fix brach Git repositáře města Plzeň
* Odpovědna osoba (admin) buď provede deploy na Dev Test a provede testovaní/kontrolu, nebo udělá merge request přímo do master branch a udělá deploy na produkčním serveru

Adresa produkční serveru:

* chatbot.plzen.eu
* chatbot.plzen.eu/admin

.. _hot_fix:
.. figure:: images/wdf_deploy_hot_fix.jpg
    :align: center

    Hot Fix


---------------------------
Start serveru
---------------------------
* Django web server ``python3 ./manage.py runserver``
* Redis message broker ``redis-server &``
* Celery worker ``celery -A bot worker``
* Celery scheduler ``celery -A bot beat``
* NLU service ``gunicorn 'botshot_nlu.server:api("model")' --bind 8008``

OR

* NLU service ``gunicorn 'botshot_nlu.server:api("model")' --bind 8008``
* Start with **bots** command ``bots start``
* * start Django, Celery and Redis

.. _GitRunner: https://docs.gitlab.com/runner/commands/Git
