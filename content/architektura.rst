###################
Architektura
###################

Celý systém se skládá z několika komponent, viz. architektura_ .

.. _architektura:
.. figure:: images/Plzen_diagram_final.jpg
    :align: center

    Architektura systému.

---------------------------
Framework
---------------------------

Samotná aplikace se skládá z několika modulů. Základní část celé aplikace je Framework Botshot_.
V této dokumentaci si nebudeme podrobně rozepisovat jednotlivé funkcionality frameworku, jelikož podrobný popis můžete najít v dokumentaci BotshotDoc_.

----------------------------------
Technologický závislosti Framework
----------------------------------

Botshot využívá Django_. Dále se pro škálovatelnost využívá Task Query Framework Celery_. Pro ukládání dat Redis_. Všechny technologie jsou součástí buildu celého systému.

------------------------------------
NLU - natural language understanding
------------------------------------
Chatbot využívá vlastní NLU, která se nachází ve složce botshot_nlu.
Pro nasazení je důležitá složka nlu, v které se nachází:

* Exmaples.txt – popis základních slovních reakcí a jejich kontext
* Topics.yml – popis tlačítek a systémových komponent a jejich kontext
* Dockerfile – aktuální dockerfile pro deploy

-----------------------------------
Popis komunikačních flow v systému
-----------------------------------

Botshot je knihovna (framework), která běží na „Django“ aplikačním serveru na adrese 0.0.0.0:8000.

(Je to podobné jako udělat WAR nebo JAR a nahrajete to na Tomcat, který to rozběhne.)

1)	Uživatel přes „FB Api nebo „chat windows“ pošle zprávu na aplikační server.
2)	Botshot tu zprávu přijme a na cachuje do „Redis“, plus vytvoří „Celery“ (celery worker), který bude obsluhovat komunikaci s uživatelem.
3)	Botshot pošle zprávu do „NLU“, která vrátí tzv. content.
4)	Botshot se podívá do Postgresql a vytáhne potřebnou historii o uživateli.
5)	Botshot vnitřní logiky a výstupy z „NLU“ a postgresql provede potřebnou operaci, např. vypíše seznam tlačítek
6)	Botshot uloží informaci do „Redis“ a zavolá službu „Celery“, která zajistí, že vezme dříve vytvořeného workera a zprávu pošle správnému uživateli přes FB Api nebo chatovací okno
7)	Botshot může provést akci, která potřebuje opoždění, např. po poslání zprávy nastavit 5min timeout a poslat zprávu typu „můžu poprosit o Váš feedback“, k tomu se použije služba „Beat“, respektive „celery beat“. „Celery beat“ vyvolí akci, najde „celery workera“ a pošle zprávu správnému uživateli přes FB Api nebo chatovací okno

Jelikož Celery, Celery Beat komunikují s Botshotem nablízko, tak je vše zakreslené v jednom kontejneru plus je tam zakreslená  „website“, jakožto spuštěný Django aplikační server.

Čili kompletní komunikaci, zpracování dat a rozhodování provádí Botshot knihovna.

-----------------------------------
Popis dockerfile a jeho součásti
-----------------------------------

Services:

redis:

*   co to je: In memory databáze
*   k čemu to je: uložiště slouží pro uložení cache dat
*   image: 'redis:3.0-alpine'
*   command: redis-server --port 6379

website:

*   co to je: Django server, neboli kontejner pro webový server
*   k čemu to je: přijímá requesty od messaging services - Facebook
*   image: $CI_REGISTRY_IMAGE/website:${TAG}
*   command: uwsgi --wsgi-file=bot/wsgi.py --http=0.0.0.0:8000 --static-map=/static=/app/static

celery:

*   co to je: Kontejner pro celery workers. Workery, které vrátí odpověď na zprávy od uživatele
*   k čemu to je: odesílání zprav
*   image: $CI_REGISTRY_IMAGE/celery:${TAG}
*   command: celery worker -A bot --concurrency=1 --loglevel=debug

nlu:

*   co to je: : Kontejner pro NLU servis, služba pro rozpoznání významu přijaté zprávy
*   k čemu to je: zpracovaní přijatých zpráv
*    image: $CI_REGISTRY_IMAGE/nlu:${TAG}
*    command: gunicorn 'botshot_nlu.server:api("model")' --bind 0.0.0.0:8008


beat:

*   co to je: Kontejner pro celery beat. Služba, která spravuje scheduled tasky, neboli plánovací úlohy
*   k čemu to je: odesílání scheduled messages
*   image: $CI_REGISTRY_IMAGE/beat:${TAG}
*   command: celery beat -A bot --loglevel=debug


postgresql:

*    image: "postgres:9.5"

client:

*   co to je: Server pro embedded chatovací okno server, který komunikuje s chatovacím oknem na webu
*   k čemu to je: komunikace chat okna s chatbotem, caching zpráv
*   image: $CI_REGISTRY_IMAGE/client:${TAG}
*   command:  gunicorn chat_client.main:app --bind 0.0.0.0:8010 --workers 1 --log-level debug


.. _Botshot: https://github.com/botshot/botshot
.. _BotshotDoc: https://botshot.readthedocs.io/en/latest/
.. _Django: https://djangoproject.com/
.. _Celery: https://celeryproject.org/
.. _Redis:  https://redis.io/

-----------------------------------
Popis gitlab variables repozitářů
-----------------------------------

Pro běh containerů musí být v repozitářích nasteveny tyto gitlab-variables.

chatbot:

*   DOCKER_HOST_HOSTING3
*   REDIS_PORT
*   FB_VERIFY_TOKEN_BETA / FB_VERIFY_TOKEN_PROD # Token, který potřebuje FB pro napojení s developers.facebool.com aplikaci pomoci WebHooks
*   FB_PAGE_NAME_BETA / FB_PAGE_NAME_PROD   # Název FB stránky, se kterou chatbot komunikuje
*   FB_PAGE_TOKEN_BETA / FB_PAGE_TOKEN_PROD # Token, který je vygenerován v developer.facebook.com pro propojení se stránkou v FB_PAGE_NAME
*   WEBHOOK_SECRET_URL_DEV  # Jedná se o URL pro napojení přes WebHooks s developers.facebool.com
*   BOTSHOT_DEPLOY_URL_BETA / BOTSHOT_DEPLOY_URL_PROD   # URL, kde běží kontejner botshot
*   CHATBASE_API_KEY_DEV    # API klíč ke službě Chatbase
*   DB_NAME_DEV
*   DB_USER_DEV
*   DB_PASSWORD_DEV
*   CHAT_WINDOW_SECRET_DEV  # heslo pro napojení na chat window
*   SUPERUSER_USERNAME_BETA / SUPERUSER_USERNAME_PROD
*   SUPERUSER_EMAIL_BETA / SUPERUSER_EMAIL_PROD
*   SUPERUSER_PASSWORD_BETA / SUPERUSER_PASSWORD_PROD

chatbot-admin:

*   DOCKER_HOST_HOSTING3
*   API_URL_BETA / API_URL_PROD

chatbot-client:

*   DOCKER_HOST_HOSTING3
*   API_URL_BETA / API_URL_PROD
*   BUNDLE_URL_BETA / BUNDLE_URL_PROD   # https://chatbotclient.plzen.eu/bundle - adresa na bundle, kde běží chatbot-client
