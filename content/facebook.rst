#############################
Facebook
#############################


---------------------------
Dev
---------------------------

* URL administrace: https://chat-plzen.devel.wdf.cz/admin/login .. bot/botbezbot
* URL testpage s custom chat oknem: https://chat-plzen.devel.wdf.cz/test-page/
* URL custom chat okna: https://chat-plzen.devel.wdf.cz/bundle/bundle.js
* FB page: https://www.facebook.com/Chatbot-M%C4%9Bsta-Plze%C5%88-634459717025233/
* FB messenger: https://www.facebook.com/messages/t/634459717025233
* id FB aplikace: Chatbot-Plzen App ID: 2497312107157020
* URL administrace botshot: https://chatbot.devel.wdf.cz/botshot/ .. bot/botbezbot
* URL administrace pro správu uživatel: https://chatbot.devel.wdf.cz/api/admin .. bot/botbezbot
* URL client API: https://client-api.devel.wdf.cz/
* URL admin API: https://chatbot.devel.wdf.cz/api

Script pro načtení custom okna::

    <script>window.onload=function(){var e=document.createElement("div");e.id="wdf-chatbot-client",
    document.body.appendChild(e);var t=document.createElement("script");t.type="text/javascript",t.src="
    https://chat-plzen.devel.wdf.cz/bundle/bundle.js",t.charset="utf-8",document.body.appendChild(t)};<
    /script>



---------------------------
Test
---------------------------

* URL administrace: https://chatbotadmin.dev.plzen.eu/admin .. bot/botbezbot
* URL testpage s custom chat oknem: https://chatbotclient.dev.plzen.eu/test-page/
* URL custom chat okna: https://chatbotclient.dev.plzen.eu/bundle/bundle.js
* FB page: https://www.facebook.com/Chatbot-Pilsen-Test-2458928227497068/
* FB messenger: https://www.facebook.com/messages/t/2458928227497068
* id FB aplikace: Chatbot-Pilsner-Test App ID: 2244802245555812
* URL administrace botshot:https://chatbot.dev.plzen.eu/botshot/ .. bot/botbezbot
* URL administrace pro správu uživatel: https://chatbot.dev.plzen.eu/api/admin/ .. bot/botbezbot
* URL client API: http://chatbotclientapi.dev.plzen.eu
* URL admin API: https://chatbot.dev.plzen.eu/api

Script pro načtení custom okna::

    <script>window.onload=function(){var e=document.createElement("div");e.id="wdf-chatbot-client",
    document.body.appendChild(e);var t=document.createElement("script");t.type="text/javascript",t.src="
    https://chatbotclient.dev.plzen.eu/bundle/bundle.js",t.charset="utf-8",document.body.appendChild
    (t)};</script>


---------------------------
Production
---------------------------

* URL administrace: https://chatbotadmin.plzen.eu/admin .. bot/botbezbot
* URL testpage s custom chat oknem: https://chatbotclient.plzen.eu/test-page/
* URL custom chat okna: https://chatbotclient.plzen.eu/bundle/bundle.js
* FB page: https://www.facebook.com/Chatbot-msta-Plzn-101717641240306/
* FB messenger: https://www.facebook.com/messages/t/101717641240306
* id FB aplikace: Chatbot-Pilsner-Prod App ID: 1457189961072594
* URL administrace botshot:https://chatbot.plzen.eu/botshot/ .. bot/botbezbot
* URL administrace pro správu uživatel: https://chatbot.plzen.eu/api/admin/ .. bot/botbezbot
* URL client API: http://chatbotclientapi.plzen.eu
* URL admin API: https://chatbot.plzen.eu/api

Script pro načtení custom okna::

    <script>window.onload=function(){var e=document.createElement("div");e.id="wdf-chatbot-client",
    document.body.appendChild(e);var t=document.createElement("script");t.type="text/javascript",t.src="
    https://chatbotclient.plzen.eu/bundle/bundle.js",t.charset="utf-8",document.body.appendChild(t)};<
    /script>